#!/bin/bash

DIR=$(pwd)
FRONT=$DIR/frontend

buildFrontend(){
   cd $FRONT
   npm i && npm run build && npm run dev 1>&2
}

serveFrontend(){
    cd $FRONT
    npm run dev 1>&2
 
}

startServer(){
./main
}


if [ -d "$FRONT/dist" ]
then
    serveFrontend & 
else
    buildFrontend &
fi


if [ -f "$DIR/main"  ]
then
    ./main &
else
    go build 
    ./main &
fi
    
