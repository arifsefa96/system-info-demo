const baseURL="http://localhost:8888"

async function getData(url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}

const fetchData = async (url) => await getData(url);

export{fetchData,baseURL}