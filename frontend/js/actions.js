import { initGauge } from "./gaugeInit.js";
import { fetchData, baseURL } from "./api.js";

function startSpeedTest() {
  const button = document.getElementById("startTest");
  const testDiv = document.getElementById("speedTestStart");
  const testResultDiv = document.getElementById("speedTestResult");
  const testResultDLS = document.getElementById("gauge-value-download");
  const testResultULS = document.getElementById("gauge-value-upload");
  const loader = document.getElementById("loader");

  loader.style.display = "inline-flex";
  button.style.display = "none";
  fetchData(`${baseURL}/network/speed`).then((res) => {
    testDiv.style.display = "none";
    testResultDiv.style.display = "inline-flex";
    setTimeout(() => {
      initGauge("download", res.downloadspeed);
      initGauge("upload", res.uploadspeed);
    }, 500);
    testResultDLS.innerHTML =
      "Download: " + parseFloat(res.downloadspeed).toFixed(2) + " Mbps";
    testResultULS.innerHTML =
      "Upload: " + parseFloat(res.uploadspeed).toFixed(2) + " Mbps";
  })
  .catch(()=>{
    button.style.display = "inline-flex";
    button.innerHTML="TRY AGAİN"
  }
  )
}

function locationHashChanged() {
  if (location.hash == "#/test") {
    setTimeout(() => {
      const startButton = document.getElementById("startTest");
      startButton.addEventListener("click", startSpeedTest, false);
    }, 500);
  }
}

export { locationHashChanged };
