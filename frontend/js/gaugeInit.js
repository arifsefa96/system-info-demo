import { Gauge } from "../utils/gauge.min.js";
var gauge = [];

var opts = {
  angle: 0.1, // The span of the gauge arc
  lineWidth: 0.4, // The line thickness
  radiusScale: 1, // Relative radius
  pointer: {
    length: 0.6, // // Relative to gauge radius
    strokeWidth: 0.055, // The thickness
    color: '#000000' // Fill color
  },
  limitMax: false,     // If false, max value increases automatically if value > maxValue
  limitMin: false,     // If true, the min value of the gauge will be fixed
  colorStart: '#6F6EA0',   // Colors
  colorStop: '#C0C0DB',    // just experiment with them
  strokeColor: '#EEEEEE',  // to see which ones work best for you
  generateGradient: true,
  highDpiSupport: true,     // High resolution support
  staticZones: [
    {strokeStyle: "#F03E3E", min: 0, max: 10}, // Red from 100 to 130
    {strokeStyle: "#FFDD00", min: 10, max: 25}, // Yellow
    {strokeStyle: "#30B32D", min: 25, max: 50}, // Green

 ],
 staticLabels: {
  font: "15px sans-serif",  // Specifies font
  labels: [10,25,50],  // Print labels at these values
  color: "#000000",  // Optional: Label text color
  fractionDigits: 0  // Optional: Numerical precision. 0=round off.
},

};


function initGauge(id,val) {
    var target = document.getElementById(id);
    gauge[id] = new Gauge(target).setOptions(opts);
    gauge[id].maxValue = 50;
    gauge[id].setMinValue(0);
    gauge[id].set(val);
  }



  export {initGauge}