import { Controller } from "./controller/controller.js";
import "./framework/mvc";
import "./style/style.scss";
import {locationHashChanged} from "./js/actions"

window.addEventListener("hashchange", locationHashChanged, false);


const controller = new Controller();
mvc.AddRoute(controller.Network, "test", "./views/networkInfo.html");
mvc.AddRoute(controller.Info, "info", "./views/info.html");
mvc.Initialize();
