import { fetchData,baseURL } from "../js/api";

var jsonData = null;
var networkData=null;


fetchData(baseURL).then((res) => {
  jsonData = res;
});

fetchData(`${baseURL}/network`).then((res) => {
  networkData = res;
});



class Controller {
  Home(model) {
    model.hostname = jsonData.hostname;
  }
  Info(model) {
    model.hostname = jsonData.hostname;
    model.systemos = jsonData.systemos;
    model.processors = jsonData.processors;
    model.hostip = jsonData.hostip;
    model.ram = jsonData.ram;
    model.diskused = jsonData.diskused.toFixed(2);
  }

  Network(model) {
    model.ip = networkData.hostip;
    model.isp = networkData.isp;
    model.latitude = networkData.location.lat;
    model.longitude = networkData.location.long;
    model.distance=parseFloat(networkData.service.distance).toFixed(2)
    model.country=networkData.service.Country
    model.sponsor=networkData.service.sponsor
    
  }
}
export { Controller };
