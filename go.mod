module main

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.8.0
	github.com/shirou/gopsutil v3.21.6+incompatible
	github.com/shirou/gopsutil/v3 v3.21.6
	github.com/showwin/speedtest-go v1.1.4
)
