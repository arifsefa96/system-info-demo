package main

import "time"

type SystemInfo struct {
	HostName   string  `json:"hostname"`
	SystemOS   string  `json:"systemos"`
	Processors string  `json:"processors"`
	Memory     string  `json:"memory"`
	HostIP     string  `json:"hostip"`
	DiskUsed   float64 `json:"diskused"`
	Ram        uint64  `json:"ram"`
}

type NetworkInfo struct {
	HostIP   string          `json:"hostip"`
	ISP      string          `json:"isp"`
	Location NetworkLocation `json:"location"`
	Service  ServiceInfo     `json:"service"`
}
type NetworkLocation struct {
	Latitude  string `json:"lat"`
	Longitude string `json:"long"`
}

type NetworkSpeed struct {
	Latency       time.Duration `json:"hostlatency"`
	UploadSpeed   float64       `json:"uploadspeed"`
	DownloadSpeed float64       `json:"downloadspeed"`
}
type ServiceInfo struct {
	Sponsor  string  `json:"sponsor"`
	Distance float64 `json:"distance"`
	Country  string  `json"country"`
}
